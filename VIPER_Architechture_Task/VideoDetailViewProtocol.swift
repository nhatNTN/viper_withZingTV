//
//  VideoDetailViewControl.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 12/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit

protocol VideoDetailViewProtocol: class {
    var presenter: VideoDetailPresenterProtocol? {get set}
    
    // PRESENTER -> VIEW
    func showVideoDetail(forVideo video: VideoModel)
}

protocol VideoDetailWireFrameProtocol: class {
    static func createVideoDetailModule(forVideo video: VideoModel) -> UIViewController
}

protocol VideoDetailPresenterProtocol: class {
    var view: VideoDetailViewProtocol? { get set }
    var wireFrame: VideoDetailWireFrameProtocol? { get set }
    var video: VideoModel? { get set }
    
    // VIEW -> PRESENTER
    func viewDidLoad()
}
