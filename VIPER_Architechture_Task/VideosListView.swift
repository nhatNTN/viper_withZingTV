//
//  VideosListView.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 11/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit
import PKHUD

class VideosListView: UIViewController {

    @IBOutlet weak var videoCollectionView: UICollectionView!
    @IBOutlet weak var filmCollectionView: UICollectionView!
    var presenter: VideosListPresenterProtocol?
    var videoList: [VideoModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationController?.navigationBar.barStyle = UIBarStyle.default
    }
}

extension VideosListView: VideosListProtocol {
    func showVideos(with videos: [VideoModel]) {
        videoList = videos
        videoCollectionView.reloadData()
        filmCollectionView.reloadData()
    }
    
    func showError() {
        HUD.flash(.label("Internet not connected"), delay: 2.0)
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func hideLoading() {
        HUD.hide()
    }
}

extension VideosListView: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == videoCollectionView{
            return 4
        }
        else {
            return videoList.count - 4
        }
    }
    
    // Chỉ định cách hiển thị từng Item của Collection View
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == videoCollectionView {
            let cell = self.videoCollectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollectionCellID", for: indexPath) as! VideoCollectionCell
            let video = videoList[indexPath.row]
            cell.set(forVideos: video)
            return cell
        }
        else {
            let cell = self.filmCollectionView.dequeueReusableCell(withReuseIdentifier: "FilmCollectionCellID", for: indexPath) as! FilmCollectionCell
            let video = videoList[indexPath.row + 4]
            cell.set(forVideos: video)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == videoCollectionView {
            presenter?.showVideoDetail(forVideo: videoList[indexPath.row])
        }
        else {
            presenter?.showVideoDetail(forVideo: videoList[indexPath.row + 4])
        }
    }
}
