//
//  VideosListRemoteDataManager.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 11/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class VideosListRemoteDataManager: VideosListRemoteDataManagerInputProtocol{
    
    var remoteRequestHandler: VideosListRemoteDataManagerOutputProtocol?
    
    func retrieveVideosList() {
        if let path = Bundle.main.path(forResource: "ZingTV", ofType: "json")
        {
            do{
                let pathAsData = try NSData(contentsOfFile: path, options: NSData.ReadingOptions.alwaysMapped)
                let json = JSON(data: pathAsData as Data)
                print(json)
                var resultVideos = [VideoModel]()
                if let videos = json["newUpdatedVideos"].arrayObject as? [[String: AnyObject]] {
                    for video in videos{
                        if let item = VideoModel(JSON: video){
                            resultVideos.append(item)
                        }
                    }
                }
                self.remoteRequestHandler?.onVideosRetrieved(resultVideos)
            } catch{
                self.remoteRequestHandler?.onError()
            }
        }
    }
}
